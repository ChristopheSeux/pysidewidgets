from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *

class Shadow(QGraphicsDropShadowEffect) :
    def __init__(self, parent=None, color=None, radius=4, offset=2) :
        super().__init__(parent)

        self.setBlurRadius(radius)

        if not isinstance(offset, (list, tuple)) :
            offset = (offset, offset)
        self.setOffset(*offset)

        if color is None :
            color = QColor(0,0,0,100)
        elif isinstance(color, int) :
            color = QColor(0,0,0,color)
        self.setColor(color)


class HSlider(QDoubleSpinBox) :
    clicked = Signal()

    def __init__(self,  parent = None, name = '', **kargs):
        super().__init__(parent)

        # Selector Name for filter in the stylesheet
        self.setObjectName('HSlider')

        self._unit = kargs['unit'] if 'unit' in kargs else ''
        self._x = 0
        self._value = 0

        self.step = kargs['step'] if 'step' in kargs else 0.1

        self.min = kargs['min'] if 'min' in kargs else -float('inf')
        self.max = kargs['max'] if 'max' in kargs else float('max')

        self.setRange(self.min, self.max)

        if 'value' in kargs :
            self.setValue(value)

        self.setDecimals(kargs['precision'] if 'precision' in kargs else 2)

        self.lineEdit().hide()
        self.setButtonSymbols(QAbstractSpinBox.NoButtons)

        # Stacked Layout for adding widgets on top of another
        self.setLayout( QStackedLayout() )
        self.layout().setStackingMode(QStackedLayout.StackAll)

        self.text_frame = QFrame()
        self.text_frame.setLayout( QHBoxLayout() )
        self.text_frame.layout().setContentsMargins(0,0,0,0)
        self.text_frame.setObjectName('HSliderTextFrame')
        self.layout().addWidget(self.text_frame)

        self.label_text = QLabel(text = name)
        self.label_text.setGraphicsEffect(Shadow(self))
        self.text_frame.layout().addWidget(self.label_text)

        self.text_frame.layout().addStretch()

        self.label_value = QLabel()
        self.label_value.setGraphicsEffect(Shadow(self))
        self.text_frame.layout().addWidget(self.label_value)

        self.slider_frame = QFrame()
        self.slider_frame.setObjectName('HSliderFrame')
        self.slider_frame.setGraphicsEffect(Shadow(self, color = 40, radius =4))
        self.layout().addWidget(self.slider_frame)

        self.setGraphicsEffect(Shadow(self, color = 35, radius =2))

        self.valueChanged.connect(self.update)

        self.update()

    @property
    def name(self) :
        self.label_text.text()

    @name.setter
    def name(self, name):
        self.label_text.setText(name)

    @property
    def unit(self) :
        return self._unit

    @unit.setter
    def unit(self, unit):
        self._unit = unit
        self.update()

    def update(self) :
        self.label_value.setText(self.lineEdit().text()+' %s'%self.unit)

        #Setting Mask on the slider_frame
        pos = self.value() / (self.maximum() - self.minimum())
        w = pos*self.width()
        if w<1 : w=1
        self.slider_frame.setMask(QRegion(0,0,w,self.height()))

    def mousePressEvent(self, event) :
        self._x = event.x()
        self._value = self.value()

    def mouseMoveEvent(self, event):
        offset = (event.x()-self._x)

        if self.min is not None and self.max is not None :
            pos = (offset/self.width() ) * (self.maximum() - self.minimum() )
        else :
            coeff = 1 if self.decimals()== 0 else 0.1
            pos = offset *coeff

        # Approximate to the lower Step
        step_offset = (pos%self.step)
        if round(step_offset, self.decimals() ) != self.step :
            pos = pos - (pos%self.step)

        self.setValue(self._value + pos)

        self.valueChanged.emit(self.value())


if __name__ == '__main__':

    import sys
    app = QApplication(sys.argv)

    window = QWidget()
    window.setLayout(QVBoxLayout() )
    window.setStyleSheet('''
        * {
            color :  rgb(220,220,220);
            font-size: 12px;
            font-weight: 500;
            border-radius : 5px;
            border-style: solid;
            background : rgb(80,80,80);
        }
        QLabel {
         background : transparent;
        }
        #HSlider {
            min-height : 24px;
            background : rgb(100,100,100);
            border-width: 1px;
            border-top-color: rgb(70,70,70);
            border-left-color: rgb(70,70,70);
            border-right-color: rgb(110,110,110);
            border-bottom-color: rgb(110,110,110);
        }
        #HSliderTextFrame {
            margin : 5px;
            background : transparent;
        }
        #HSliderFrame {
            background : rgb(85,127,193);
            border-width: 1px;
            border-top-color: rgb(90,132,198);
            border-left-color: rgb(90,132,198);
            border-right-color: rgb(80,122,188);
            border-bottom-color: rgb(80,122,188);
        }
        ''')

    slider = HSlider(name = 'Slider', min = 0, max = 5, unit = 'px')
    window.layout().addWidget(slider)

    window.show()

    sys.exit(app.exec_())
